# conveyor-protocol

Gloss protocol for TCP communication between Conveyor server and client
applications. The protocol is designed to be imported by both Conveyor
and client applications wishing to take advantage of the TCP protocol. In the
future, we may have two namespaces - one for client-initiated messages and one
for server-initiated messaged. Currently there is no programatic indication as
to which side should generate the messages.

## License

Copyright © 2017 Lambda Software Solutions

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
