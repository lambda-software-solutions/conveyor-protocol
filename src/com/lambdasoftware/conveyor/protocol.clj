(ns com.lambdasoftware.conveyor.protocol
  (:require [gloss.core :as gloss]
            [gloss.io :as gio]
            [manifold.stream :as s]))

;; Variable-length UTF-8 string
(def var-str
  (gloss/finite-frame :int32 (gloss/string :utf-8)))

(def bin-data
  (gloss/finite-block :int32))

(gloss/defcodec frame-type
  (gloss/enum :byte
              :begin-tx :commit-tx :rollback-tx
              :event :create-event
              :feed :get-feed :create-feed :subscribe-feed
              :feed-created :event-created :subscribed-to-feed
              :error :client-disconnect :server-disconnect :heartbeat))

(gloss/defcodec error-code
  (gloss/enum :byte
              :feed-not-found :event-not-found :duplicate-entry :server-busy
              :unknown-error))

(def event-spec
  {:feed var-str
   :ts :uint64
   :tx-id :uint64
   :data bin-data})

(gloss/defcodec event (assoc event-spec
                             :type :event
                             :key :int32))

(gloss/defcodec create-event
  {:type :create-event
   :feed var-str
   :data bin-data
   :tmpid :int32
   :key :int32})

(gloss/defcodec begin-tx
  {:type :begin-tx
   :tmpid :int32
   :key :int32})

(gloss/defcodec commit-tx
  {:type :commit-tx
   :tmpid :int32
   :key :int32})

(gloss/defcodec rollback-tx
  {:type :rollback-tx
   :tmpid :int32
   :key :int32})

(gloss/defcodec feed
  {:type :feed
   :feed var-str
   :events (gloss/repeated event-spec
                           :prefix :int16)
   :key :int32})

(gloss/defcodec get-feed
  {:type :get-feed
   :feed var-str
   :before-ts :int64
   :after-ts :int64
   :before-tx :int64
   :after-tx :int64
   :key :int32})

(gloss/defcodec create-feed
  {:type :create-feed
   :feed var-str
   :key :int32})

(gloss/defcodec subscribe-feed
  {:type :subscribe-feed
   :feed var-str
   :key :int32})

(gloss/defcodec feed-created
  {:type :feed-created
   :feed var-str
   :key :int32})

(gloss/defcodec event-created
  {:type :event-created
   :feed var-str
   :tx-id :uint64
   :key :int32})

(gloss/defcodec subscribed-to-feed
  {:type :subscribed-to-feed
   :feed var-str
   :key :int32})

(gloss/defcodec error
  {:type :error
   :code error-code
   :message var-str
   :key :int32})

(gloss/defcodec client-disconnect
  {:type :client-disconnect
   :message var-str})

(gloss/defcodec server-disconnect
  {:type :server-disconnect
   :message var-str})

(gloss/defcodec heartbeat
  {:type :heartbeat
   :order :int32})

(def type->codec
  {:event event
   :create-event create-event
   :begin-tx begin-tx
   :commit-tx commit-tx
   :rollback-tx rollback-tx
   :feed feed
   :get-feed get-feed
   :create-feed create-feed
   :subscribe-feed subscribe-feed
   :feed-created feed-created
   :event-created event-created
   :subscribed-to-feed subscribed-to-feed
   :error error
   :client-disconnect client-disconnect
   :server-disconnect server-disconnect
   :heartbeat heartbeat})

(gloss/defcodec protocol
  (gloss/header frame-type type->codec :type))

(defn wrap-encoded-duplex-stream
  ([stream] (wrap-encoded-duplex-stream stream protocol))
  ([stream frame]
   (let [out (s/stream)]
     (s/connect
      (s/map #(gio/encode frame %) out)
      stream)
     (s/splice
      out
      (gio/decode-stream stream frame)))))

(defn unpack-payload
  "Takes a sequence of ByteBuffers that represent an array of raw bytes
  and converts them into a byte array. This can be used for getting a byte
  array from a field encoded as bin-data."
  [buffers]
  (let [buff (gio/contiguous buffers)
        arr (make-array Byte/TYPE (.remaining buff))]
    (.get buff arr)
    arr))
