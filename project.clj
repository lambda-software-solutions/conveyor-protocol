(defproject com.lambdasoftware.conveyor/protocol "1.2.0"
  :description "Gloss protocol for Conveyor event sourcing client-server communication"
  :url "https://gitlab.com/lambda-software-solutions/conveyor-protocol"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [gloss "0.2.5"]
                 [potemkin "0.4.3"]
                 [manifold "0.1.4"]])
